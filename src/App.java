import com.devcamp.Circle;
import com.devcamp.Cylinder;

public class App {
    public static void main(String[] args) throws Exception {
        //System.out.println("Hello, World!");
        
        Circle circle1= new Circle();
        Circle circle2= new Circle(2.0);
        Circle circle3= new Circle(3.0, "green");
        System.out.println(circle1);
        System.out.println(circle2);
        System.out.println(circle3);

        System.out.println("Dien tich circle1= "+circle1.getArea());
        System.out.println("Dien tich circle2= "+circle2.getArea());
        System.out.println("Dien tich circle3= "+circle3.getArea());
 

        Cylinder cylinder1= new Cylinder();
        Cylinder cylinder2= new Cylinder(2.5);
        Cylinder cylinder3= new Cylinder(3.5, 1.5);
        Cylinder cylinder4= new Cylinder(3.5 , "green", 1.5);

        System.out.println(cylinder1);
        System.out.println(cylinder2);
        System.out.println(cylinder3);
        System.out.println(cylinder4);

        System.out.println("dien tich hinh tru 1= "+cylinder1.getVolume());
        System.out.println("dien tich hinh tru 2= "+cylinder2.getVolume());
        System.out.println("dien tich hinh tru 3= "+cylinder3.getVolume());
        System.out.println("dien tich hinh tru 4= "+cylinder4.getVolume());

    }
}
